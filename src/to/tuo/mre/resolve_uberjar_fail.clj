;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

(ns to.tuo.mre.resolve-uberjar-fail
  (:gen-class))

;; TODO is this the best way to find the current ns?  *ns* is dynamic,
;; so at runtime execution of fn body, will not necessarily be bound
;; to the ns to.tuo.mre.resolve-uberjar-fail
(def my-ns *ns*)

;; run with
;; clojure -M -m to.tuo.mre.resolve-uberjar-fail
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  ;; TODO: which of the following lines is better?  grabbing the ns
  ;; from the meta of the var -main seems less brittle.  also does not
  ;; pollute the ns with 'my-ns.  but it is more verbose.  maybe *ns*
  ;; is guaranteed to be the right ns during compile time, so it's no
  ;; more or less brittle than grabbing it out of the meta
  (->> '-main (ns-resolve (-> #'-main meta :ns)) println)
  (println (ns-resolve my-ns '-main)))

;; This file is part of mre-resolve-uberjar-fail.

;; mre-resolve-uberjar-fail is free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.

;; mre-resolve-uberjar-fail is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public
;; License along with mre-resolve-uberjar-fail.  If not, see
;; <http://www.gnu.org/licenses/>.

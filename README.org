* to.tuo.mre.resolve-uberjar-fail

minimum reproducible example of resolve returning nil when app is run
as an uberjar and not when run in cider repl

** Installation

Download from [[https://gitlab.com/ozzloy/to.tuo.mre.resolve-uberjar-fail]]

** Usage

FIXME: explanation

Run the project directly:

#+begin_src bash
clojure -M -m to.tuo.mre.resolve-uberjar-fail
#+end_src

Run the project's tests (they'll fail until you edit them):

#+begin_src bash
clojure -M:test:runner
#+end_src

Build an uberjar:

#+begin_src bash
clojure -M:uberjar
#+end_src

Run that uberjar:

#+begin_src bash
java -jar to.tuo.mre.resolve-uberjar-fail.jar
#+end_src

** Options

FIXME: listing of options this app accepts.

** Examples

...

*** Bugs

...

*** Any Other Sections
*** That You Think
*** Might be Useful

** License

Copyright © 2020 Ozzloy

Distributed under the GNU Affero General Public License either version
 3.0 or (at your option) any later version.
